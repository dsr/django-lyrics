from django.db import models
from django.forms import ModelForm
from taggit.managers import TaggableManager
from django.contrib.auth.models import User
# Create your models here.


class Lyric(models.Model):
    date_posted = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    author = models.CharField(max_length=250)
    posted_by = models.CharField(max_length=250)
    tag_list = TaggableManager()

    def get_absolute_url(self):
        return ('/')


class LyricForm(ModelForm):
    class Meta:
        model = Lyric


class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ['password']
