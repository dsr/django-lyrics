from lyrics.models import Lyric, LyricForm, UserForm
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.models import User
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.http import HttpResponseRedirect


def index(request):
    lyrics = Lyric.objects.all().order_by("-date_posted")
    paginator = Paginator(lyrics, 5, False)
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    try:
        lyric_list = paginator.page(page)
    except (InvalidPage, EmptyPage):
        lyric_list = paginator.page(paginator.num_pages)

    return render_to_response("index.html", {
        "lyric_list": lyric_list,
        },
        context_instance=RequestContext(request))


class IndexView(ListView):
    template_name = 'index.html'
    context_object_name = 'lyric_list'
    model = Lyric

    def queryset(self):
        all_lyrics = Lyric.objects.all().order_by("-date_posted")
        return all_lyrics


def detail_view(request, pk):
    lyric = Lyric.objects.get(pk=pk)
    tags = lyric.tag_list.all()
    return render_to_response('detail.html', {
        "lyric_list": [lyric],
        "tags": tags},
        context_instance=RequestContext(request))


class AddView(CreateView):
    template_name = 'add.html'
    form_class = LyricForm


class ModifyView(UpdateView):
    model = Lyric
    template_name = 'add.html'


class RemoveView(DeleteView):
    model = Lyric
    template_name = 'confirm.html'
    success_url = '/'


class AuthorSearch(ListView):
    context_object_name = "lyric_list"
    template_name = "detail.html"

    def queryset(self):
        results = []
        lyrics = Lyric.objects.all()
        for lyric in lyrics:
            lyric_authors = lyric.author.split(",")
            for author in lyric_authors:
                if author == self.args[0]:
                    results.append(lyric)
   #return Lyric.objects.filter(tags__icontains= self.args[0])
        return results


class PosterSearch(ListView):
    """docstring for PosterSearch"""
    context_object_name = 'lyric_list'
    template_name = 'detail.html'

    def queryset(self):
        lyrics = Lyric.objects.filter(posted_by=self.args[0])
        return lyrics


def tag_search(request, term):
    results = Lyric.objects.filter(tag_list__name__in=[term])
    return render_to_response('detail.html', {
        "lyric_list": results,
        "term": term},
        context_instance=RequestContext(request))


def add_user(request):
    """add a user"""
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password2']
        email = request.POST['email']
        new_user = User.objects.create_user(username=username, \
                                            email=email, password=password)
        new_user.is_staff = True
        new_user.groups = [1]
        new_user.save()
        #form = UserCreationForm(request.POST)
        #if form.is_valid():
        #    form.save()
        # return HttpResponseRedirect('/login')
        return render_to_response("registration/login.html", \
            {"just_registered": True},
        context_instance=RequestContext(request))

    else:

        user_form = UserCreationForm()
        return render_to_response("add_user.html",
            {"form": user_form}, context_instance=RequestContext(request))


def update_user(request):
    if request.method == 'POST':
        if request.POST['password1'] == request.POST['password2']:
            new_pass = request.POST['new_password']
            new_email = request.POST['new_email']
            u = User.objects.get(username__exact=request.user.username)
            if new_pass != "":
                u.set_password(new_pass)
                u.save()
            if new_email != "":
                u.email = new_email
                u.save()
            return render_to_response("success.html")
    user_form = UserForm()
    return render_to_response("user_update.html", {
            "user_form": user_form},
           context_instance=RequestContext(request))


def add_lyric(request):
    if request.method == "POST":
        form = LyricForm(request.POST)
        if form.is_valid():
            lyric = Lyric()
            lyric.author = request.POST['author']
            lyric.posted_by = request.POST['posted_by']
            lyric.text = request.POST['text']
            lyric.posted_by = request.user.username
            lyric.save()
            for tag in request.POST['tag_list'].split(","):
                lyric.tag_list.add(tag.lower().strip())
            return HttpResponseRedirect("/")
    else:
        form = LyricForm()
        return render_to_response("add.html", {
            "form": form},
            context_instance=RequestContext(request))
