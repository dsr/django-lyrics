from django.conf.urls.defaults import *
from django.conf import settings
import os
from settings import SITE_ROOT
from django.contrib.auth.views import login, logout
# Uncomment the next two lines to enable the admin:
from lyrics.views import IndexView, AddView, ModifyView, RemoveView,\
    AuthorSearch, PosterSearch
from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
    url(r'^register/', 'lyrics.views.add_user', name='register'),
    url(r'^new/', AddView.as_view(), name='lyric_add'),
    # url(r'^$', IndexView.as_view(), name='index'),
    url(r'^$', "lyrics.views.index" , name='index'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'(?P<pk>\d+)/modify/$', ModifyView.as_view(), name='change_lyric'),
    url(r'(?P<pk>\d+)/delete/$', RemoveView.as_view(), name='delete_lyric'),
    url(r'^users/', include('smartmin.users.urls')),
    url(r'^login/', login),
    url(r'^logout/$', logout, {'next_page': "/"}),
    url(r'^tag/(?P<term>.*)/$', 'lyrics.views.tag_search', name="tag_search"),
    # url(r'^tag/(.*)/$', TagSearch.as_view(), name = "tag_search"),
    url(r'^author/(.*)/$', AuthorSearch.as_view(), name="author_search"),
    url(r'^detail/(?P<pk>\d+)/$', 'lyrics.views.detail_view',\
            name="detail_view"),
    url(r'^posted_by/(.*)/$', PosterSearch.as_view(), name='poster_search'),
    url(r'^update_user/$', ('lyrics.views.update_user'), name='update_user'),
    url(r'^add_lyric/$', ('lyrics.views.add_lyric'), name='add_lyric')
)


urlpatterns += patterns('',
    (r'^static/(?P<path>.*)$', 'django.views.static.serve',
     {'document_root': os.path.join(SITE_ROOT, 'static')}),
)
